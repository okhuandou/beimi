/*
Navicat MySQL Data Transfer

Source Server         : UCKeFu
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : beimi

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-03-26 17:08:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `bm_account_config`
-- ----------------------------
DROP TABLE IF EXISTS `bm_account_config`;
CREATE TABLE `bm_account_config` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `username` varchar(32) DEFAULT NULL COMMENT '创建人用户名',
  `name` varchar(32) DEFAULT NULL COMMENT '名称',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `enableask` tinyint(4) DEFAULT NULL COMMENT '允许AI主动发起问答',
  `askfirst` tinyint(4) DEFAULT NULL COMMENT 'AI优先',
  `enablescene` tinyint(4) DEFAULT NULL COMMENT '启用场景识别',
  `scenefirst` tinyint(4) DEFAULT NULL COMMENT '优先命中场景',
  `enablekeyword` tinyint(4) DEFAULT NULL COMMENT '启用关键词命中',
  `keywordnum` int(11) DEFAULT NULL COMMENT '关键词数量',
  `noresultmsg` text COMMENT '未命中回复消息',
  `askqs` tinyint(4) DEFAULT NULL COMMENT '询问访客是否解决问题',
  `asktipmsg` varchar(255) DEFAULT NULL COMMENT '询问访客的文本',
  `resolved` varchar(100) DEFAULT NULL COMMENT '已解决的提示文本',
  `unresolved` varchar(100) DEFAULT NULL COMMENT '未解决的提示文本',
  `redirectagent` tinyint(4) DEFAULT NULL COMMENT '跳转到人工坐席',
  `redirecturl` varchar(255) DEFAULT NULL COMMENT '跳转到其他URL',
  `asktimes` int(11) DEFAULT NULL COMMENT '最长多久开始询问',
  `selectskill` int(11) DEFAULT NULL,
  `selectskillmsg` varchar(255) DEFAULT NULL,
  `expdays` int(11) DEFAULT NULL,
  `initaccount` tinyint(4) DEFAULT NULL,
  `initcoins` int(11) DEFAULT NULL,
  `initcards` int(11) DEFAULT NULL,
  `initdiamonds` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_account_config
-- ----------------------------
INSERT INTO `bm_account_config` VALUES ('402888815e058149015e0581dbe40000', 'beimi', '297e8c7b455798280145579c73e501c1', null, null, '2017-08-21 23:55:50', '0', '0', '0', '0', '0', '5', '', '0', '您好，小E是否已经解决了您的问题？', '已解决（关闭对话）', '未解决（转人工坐席）', '0', null, '120', '0', '请选择您要咨询的问题分类？', '0', '1', '5000', '10', '10');

-- ----------------------------
-- Table structure for `bm_attachment_file`
-- ----------------------------
DROP TABLE IF EXISTS `bm_attachment_file`;
CREATE TABLE `bm_attachment_file` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人ID',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `organ` varchar(32) DEFAULT NULL COMMENT '组织机构ID',
  `datastatus` tinyint(4) DEFAULT NULL COMMENT '数据状态（逻辑删除）',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `url` varchar(255) DEFAULT NULL COMMENT '地址',
  `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
  `filelength` int(11) DEFAULT NULL COMMENT '文件长度',
  `filetype` varchar(255) DEFAULT NULL COMMENT '文件类型',
  `image` tinyint(4) DEFAULT NULL COMMENT '是否图片',
  `dataid` varchar(32) DEFAULT NULL COMMENT '数据ID',
  `model` varchar(32) DEFAULT NULL COMMENT '所属功能模块',
  `fileid` varchar(32) DEFAULT NULL COMMENT '文件ID',
  `modelid` varchar(32) DEFAULT NULL COMMENT '所属模块数据ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_attachment_file
-- ----------------------------
INSERT INTO `bm_attachment_file` VALUES ('402888815c73b8b7015c73c475070008', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-04 23:41:13', '402883965c1dfe92015c1e1291900003', '0', '11.png', null, '2017-06-04 23:41:13', '201306', 'image/png', '1', '5894953fb0fb4dae86b805ad81fda220', 'workorders', '4e16a00b0faa780bbe3b3d6b9df20148', '5894953fb0fb4dae86b805ad81fda220');
INSERT INTO `bm_attachment_file` VALUES ('402888815c73b8b7015c73c475170009', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-04 23:41:13', '402883965c1dfe92015c1e1291900003', '0', '11.png', null, '2017-06-04 23:41:13', '201306', 'image/png', '1', '5894953fb0fb4dae86b805ad81fda220', 'workorders', '4e16a00b0faa780bbe3b3d6b9df20148', '5894953fb0fb4dae86b805ad81fda220');
INSERT INTO `bm_attachment_file` VALUES ('402888815c73b8b7015c73c4afb6000a', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-04 23:41:28', '402883965c1dfe92015c1e1291900003', '0', '捷通华声智能外呼系统解决方案（2016.9.19）.pptx', null, '2017-06-04 23:41:28', '13161290', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', '0', '5894953fb0fb4dae86b805ad81fda220', 'workorders', '34b6bf2000fd6e7e95164be5d85562ac', '5894953fb0fb4dae86b805ad81fda220');
INSERT INTO `bm_attachment_file` VALUES ('402888815c73b8b7015c73c571c9000b', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-04 23:42:18', '402883965c1dfe92015c1e1291900003', '0', 'z3A8_ClTnR6PthP9GHpjlbEl4xcxOW42wzqEw12SbHFJQsmlbjFe2XmwDAqxL2sG4520996343697988632.jpg', null, '2017-06-04 23:42:18', '42235', 'image/jpeg', '1', '33bba4b817324488aa6a9a8dc7f6dc47', 'workorders', '8986587e53473082089fc9f8657dcfe4', '5894953fb0fb4dae86b805ad81fda220');
INSERT INTO `bm_attachment_file` VALUES ('402888815c73b8b7015c73c571e2000c', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-04 23:42:18', '402883965c1dfe92015c1e1291900003', '0', '1.png', null, '2017-06-04 23:42:18', '193645', 'image/png', '1', '33bba4b817324488aa6a9a8dc7f6dc47', 'workorders', '99999a1baa7212603be1070c55152c61', '5894953fb0fb4dae86b805ad81fda220');
INSERT INTO `bm_attachment_file` VALUES ('402888815c75bfbf015c75c4560a0008', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 09:00:20', '402883965c1dfe92015c1e1291900003', '0', '1.png', null, '2017-06-05 09:00:20', '193645', 'image/png', '1', '5de373b1e76b4d329f812d53456fb87c', 'workorders', '99999a1baa7212603be1070c55152c61', '5894953fb0fb4dae86b805ad81fda220');
INSERT INTO `bm_attachment_file` VALUES ('402888815c75bfbf015c75c456260009', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 09:00:20', '402883965c1dfe92015c1e1291900003', '0', '2.png', null, '2017-06-05 09:00:20', '169254', 'image/png', '1', '5de373b1e76b4d329f812d53456fb87c', 'workorders', '17dceb992de70f7ed83ed1040484065a', '5894953fb0fb4dae86b805ad81fda220');
INSERT INTO `bm_attachment_file` VALUES ('402888815c75bfbf015c75c45635000a', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 09:00:20', '402883965c1dfe92015c1e1291900003', '0', '3.png', null, '2017-06-05 09:00:20', '89999', 'image/png', '1', '5de373b1e76b4d329f812d53456fb87c', 'workorders', 'd16e2fb4a152072191ccf658185e7eaa', '5894953fb0fb4dae86b805ad81fda220');
INSERT INTO `bm_attachment_file` VALUES ('402888815c75bfbf015c75c4563f000b', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 09:00:20', '402883965c1dfe92015c1e1291900003', '0', '4.png', null, '2017-06-05 09:00:20', '66731', 'image/png', '1', '5de373b1e76b4d329f812d53456fb87c', 'workorders', '034921687e3058a30da01a174670e965', '5894953fb0fb4dae86b805ad81fda220');
INSERT INTO `bm_attachment_file` VALUES ('402888815c75bfbf015c75c4564b000c', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 09:00:20', '402883965c1dfe92015c1e1291900003', '0', '5.png', null, '2017-06-05 09:00:20', '68597', 'image/png', '1', '5de373b1e76b4d329f812d53456fb87c', 'workorders', 'fc7d14a26ccfa6b08f3012c7eecc5d37', '5894953fb0fb4dae86b805ad81fda220');
INSERT INTO `bm_attachment_file` VALUES ('402888815c75bfbf015c75d3dd800014', 'ukewo', '402883965c1dfe92015c1e12651d0002', '2017-06-05 09:17:18', '402883965c1dfe92015c1e1291900003', '0', '16.png', null, '2017-06-05 09:17:18', '188751', 'image/png', '1', '85406acb810f4103833da3d944d791a0', 'workorders', 'a954290ba95a6e50174869d551ed0970', '5894953fb0fb4dae86b805ad81fda220');
INSERT INTO `bm_attachment_file` VALUES ('402888815c75bfbf015c75d3f8760018', 'ukewo', '402883965c1dfe92015c1e12651d0002', '2017-06-05 09:17:24', '402883965c1dfe92015c1e1291900003', '0', '17.png', null, '2017-06-05 09:17:24', '204169', 'image/png', '1', '76686551b9d248a191e69d139f687b17', 'workorders', 'd0cb4b885c21638635ec7ecc52c0693e', 'cefcfdbb10b2413aa7da50a3eadb6e34');
INSERT INTO `bm_attachment_file` VALUES ('402888815c75bfbf015c75d42a43001e', 'ukewo', '402883965c1dfe92015c1e12651d0002', '2017-06-05 09:17:37', '402883965c1dfe92015c1e1291900003', '0', '17.png', null, '2017-06-05 09:17:37', '204169', 'image/png', '1', '8361ceac16d24e1db89ce9fd76a3e24f', 'workorders', 'd0cb4b885c21638635ec7ecc52c0693e', '01ac33e06e0c4479b315ea623f6688b9');
INSERT INTO `bm_attachment_file` VALUES ('402888815c760cc5015c760d8e760001', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 10:20:18', '402883965c1dfe92015c1e1291900003', '0', '17.png', null, '2017-06-05 10:20:18', '204169', 'image/png', '1', 'ca9d9189dfef4abbacdf36c249b477d0', 'workorders', 'd0cb4b885c21638635ec7ecc52c0693e', 'ca9d9189dfef4abbacdf36c249b477d0');
INSERT INTO `bm_attachment_file` VALUES ('402888815c760cc5015c760d8e950002', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 10:20:18', '402883965c1dfe92015c1e1291900003', '0', '3.png', null, '2017-06-05 10:20:18', '89999', 'image/png', '1', 'ca9d9189dfef4abbacdf36c249b477d0', 'workorders', 'd16e2fb4a152072191ccf658185e7eaa', 'ca9d9189dfef4abbacdf36c249b477d0');
INSERT INTO `bm_attachment_file` VALUES ('402888815c760cc5015c760d8ea60003', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 10:20:18', '402883965c1dfe92015c1e1291900003', '0', '8.png', null, '2017-06-05 10:20:18', '124295', 'image/png', '1', 'ca9d9189dfef4abbacdf36c249b477d0', 'workorders', 'b70bb5099910f99bd8ab4bdbf7ea6f19', 'ca9d9189dfef4abbacdf36c249b477d0');
INSERT INTO `bm_attachment_file` VALUES ('402888815c760cc5015c760e4f6f0005', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 10:21:08', '402883965c1dfe92015c1e1291900003', '0', '5.png', null, '2017-06-05 10:21:08', '68597', 'image/png', '1', '346a195eed934e9b910d858b62c4dc9c', 'workorders', 'fc7d14a26ccfa6b08f3012c7eecc5d37', '346a195eed934e9b910d858b62c4dc9c');
INSERT INTO `bm_attachment_file` VALUES ('402888815c760cc5015c760e4f800006', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 10:21:08', '402883965c1dfe92015c1e1291900003', '0', '6.png', null, '2017-06-05 10:21:08', '158052', 'image/png', '1', '346a195eed934e9b910d858b62c4dc9c', 'workorders', '09def84e6927bb5d75d0b0459f330ac9', '346a195eed934e9b910d858b62c4dc9c');
INSERT INTO `bm_attachment_file` VALUES ('402888815c760cc5015c761a90010009', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 10:34:31', '402883965c1dfe92015c1e1291900003', '0', '16.png', null, '2017-06-05 10:34:31', '188751', 'image/png', '1', '0debd45ab2b2435ab41fc1f12cbb863c', 'workorders', 'a954290ba95a6e50174869d551ed0970', '346a195eed934e9b910d858b62c4dc9c');
INSERT INTO `bm_attachment_file` VALUES ('402888815c761c97015c761e30650002', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 10:38:28', '402883965c1dfe92015c1e1291900003', '0', '16.png', null, '2017-06-05 10:38:28', '188751', 'image/png', '1', '56d8fe6c0d634900a3163116a7155870', 'workorders', 'a954290ba95a6e50174869d551ed0970', '346a195eed934e9b910d858b62c4dc9c');
INSERT INTO `bm_attachment_file` VALUES ('402888815c761c97015c761e30840003', 'ukewo', '297e8c7b455798280145579c73e501c1', '2017-06-05 10:38:28', '402883965c1dfe92015c1e1291900003', '0', '7.png', null, '2017-06-05 10:38:28', '258693', 'image/png', '1', '56d8fe6c0d634900a3163116a7155870', 'workorders', '908d198579b97873a5c3681bdecf2a53', '346a195eed934e9b910d858b62c4dc9c');

-- ----------------------------
-- Table structure for `bm_datadic`
-- ----------------------------
DROP TABLE IF EXISTS `bm_datadic`;
CREATE TABLE `bm_datadic` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `TITLE` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `STATUS` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `CREATER` varchar(255) DEFAULT NULL,
  `PUBLISHEDTYPE` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `TABTYPE` varchar(32) DEFAULT NULL,
  `DSTYPE` varchar(32) DEFAULT NULL,
  `DSTEMPLET` varchar(255) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT NULL,
  `DICTYPE` varchar(32) DEFAULT NULL,
  `ICONCLASS` varchar(100) DEFAULT NULL,
  `CSSSTYLE` varchar(255) DEFAULT NULL,
  `AUTHCODE` varchar(100) DEFAULT NULL,
  `DEFAULTMENU` tinyint(4) DEFAULT NULL,
  `DATAID` varchar(32) DEFAULT NULL,
  `DICICON` varchar(32) DEFAULT NULL,
  `CURICON` varchar(32) DEFAULT NULL,
  `BGCOLOR` varchar(32) DEFAULT NULL,
  `CURBGCOLOR` varchar(32) DEFAULT NULL,
  `MENUPOS` varchar(32) DEFAULT NULL,
  `DISTITLE` varchar(100) DEFAULT NULL,
  `NAVMENU` tinyint(4) DEFAULT '0',
  `QUICKMENU` tinyint(4) DEFAULT '0',
  `PROJECTID` varchar(32) DEFAULT NULL,
  UNIQUE KEY `SQL121227155530400` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_datadic
-- ----------------------------

-- ----------------------------
-- Table structure for `bm_gameconfig`
-- ----------------------------
DROP TABLE IF EXISTS `bm_gameconfig`;
CREATE TABLE `bm_gameconfig` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键ID',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `username` varchar(32) DEFAULT NULL COMMENT '用户名',
  `name` varchar(32) DEFAULT NULL COMMENT '名称',
  `sessionmsg` varchar(255) DEFAULT NULL COMMENT '会话消息',
  `distribution` varchar(32) DEFAULT NULL COMMENT '坐席分配策略',
  `timeoutmsg` varchar(255) DEFAULT NULL COMMENT '超时提醒消息',
  `retimeoutmsg` varchar(255) DEFAULT NULL COMMENT '再次超时提醒消息',
  `satisfaction` tinyint(4) DEFAULT NULL COMMENT '启用满意度调查',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `lastagent` tinyint(4) DEFAULT NULL COMMENT '最后服务坐席优先分配',
  `sessiontimeout` tinyint(4) DEFAULT NULL COMMENT '会话超时时间',
  `resessiontimeout` tinyint(4) DEFAULT NULL COMMENT '再次超时时间',
  `timeout` int(11) DEFAULT NULL COMMENT '超时时长',
  `retimeout` int(11) DEFAULT NULL COMMENT '再次超时时长',
  `maxuser` int(11) DEFAULT NULL COMMENT '最大用户数',
  `initmaxuser` int(11) DEFAULT NULL COMMENT '首次就绪分配用户数',
  `workinghours` text COMMENT '工作时间段',
  `notinwhmsg` text COMMENT '非工作时间提醒消息',
  `hourcheck` tinyint(4) DEFAULT NULL COMMENT '启用工作时间',
  `gametype` text,
  `gamemodel` varchar(32) DEFAULT NULL,
  `hallgametype` text,
  `waittime` int(11) DEFAULT NULL,
  `anysdk` tinyint(4) DEFAULT '0',
  `anysdkpay` tinyint(4) DEFAULT '0',
  `anysdkshare` tinyint(4) DEFAULT '0',
  `anysdklogin` tinyint(4) DEFAULT '0',
  `oauthserver` varchar(255) DEFAULT NULL,
  `appkey` varchar(150) DEFAULT NULL,
  `appsecret` varchar(200) DEFAULT NULL,
  `privatekey` varchar(200) DEFAULT NULL,
  `subsidy` tinyint(4) DEFAULT NULL,
  `subtimes` int(11) DEFAULT NULL,
  `subgolds` int(11) DEFAULT NULL,
  `submsg` varchar(255) DEFAULT NULL,
  `recmsg` varchar(255) DEFAULT NULL,
  `subovermsg` varchar(255) DEFAULT NULL,
  `nosubmsg` varchar(255) DEFAULT NULL,
  `welfare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_gameconfig
-- ----------------------------
INSERT INTO `bm_gameconfig` VALUES ('402888815e663eca015e6640058e0001', 'beimi', '297e8c7b455798280145579c73e501c1', null, null, null, null, '未匹配到玩家，房间已解散', null, '0', '2018-03-09 17:50:33', '0', '1', '0', '120', '120', '10', '10', null, null, '0', '402888815e0521d8015e052341f70001,402888815e0521d8015e052342080002', 'hall', null, null, '1', '0', '0', '0', '', '', '', '', '1', '3', '4000', '亲，金币没有了，您可以先领取救济金！', '亲，您的金币不足，请充值。', '您今天的救济次数已用完，请充值。', '请，您没有救济次数啦！', 'turn,over,sign');

-- ----------------------------
-- Table structure for `bm_game_ai`
-- ----------------------------
DROP TABLE IF EXISTS `bm_game_ai`;
CREATE TABLE `bm_game_ai` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键ID',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `username` varchar(32) DEFAULT NULL COMMENT '用户名',
  `name` varchar(32) DEFAULT NULL COMMENT '名称',
  `sessionmsg` varchar(255) DEFAULT NULL COMMENT '会话消息',
  `distribution` varchar(32) DEFAULT NULL COMMENT '坐席分配策略',
  `timeoutmsg` varchar(255) DEFAULT NULL COMMENT '超时提醒消息',
  `retimeoutmsg` varchar(255) DEFAULT NULL COMMENT '再次超时提醒消息',
  `satisfaction` tinyint(4) DEFAULT NULL COMMENT '启用满意度调查',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `timeout` int(11) DEFAULT NULL COMMENT '超时时长',
  `retimeout` int(11) DEFAULT NULL COMMENT '再次超时时长',
  `maxai` int(11) DEFAULT NULL COMMENT '最大AI数量',
  `initmaxuser` int(11) DEFAULT NULL COMMENT '首次就绪分配用户数',
  `hourcheck` tinyint(4) DEFAULT NULL COMMENT '启用工作时间',
  `gametype` text,
  `gamemodel` varchar(32) DEFAULT NULL,
  `hallgametype` text,
  `enableai` tinyint(4) DEFAULT NULL COMMENT '是否启用AI',
  `waittime` int(11) DEFAULT NULL COMMENT '玩家等待时长',
  `initcoins` int(11) DEFAULT NULL COMMENT '初始化金币',
  `initcards` int(11) DEFAULT NULL COMMENT '初始化房卡',
  `initdiamonds` int(11) DEFAULT NULL COMMENT '初始化钻石',
  `exitcon` varchar(32) DEFAULT NULL COMMENT '退出条件',
  `dicinfo` tinyint(4) DEFAULT NULL,
  `aichat` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_game_ai
-- ----------------------------
INSERT INTO `bm_game_ai` VALUES ('40288881600d551801600d55a1780000', 'beimi', '297e8c7b455798280145579c73e501c1', null, null, null, null, null, null, null, '2017-12-08 08:42:38', null, null, '0', null, null, null, null, null, '1', '5', '8000', '10', '10', 'bank', '1', '1');

-- ----------------------------
-- Table structure for `bm_game_group`
-- ----------------------------
DROP TABLE IF EXISTS `bm_game_group`;
CREATE TABLE `bm_game_group` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `TITLE` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `STATUS` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `CREATER` varchar(255) DEFAULT NULL,
  `PLAYWAYID` varchar(32) DEFAULT NULL,
  `GAME` varchar(32) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT '0',
  `STYLE` varchar(32) DEFAULT NULL,
  UNIQUE KEY `SQL121227155530400` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_game_group
-- ----------------------------
INSERT INTO `bm_game_group` VALUES ('4028888160297cb001602980d6180000', '确定地主', null, 'dizhu', null, 'radio', null, 'beimi', null, '2017-12-06 23:03:42', '2017-12-06 23:03:42', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '1', 'four');
INSERT INTO `bm_game_group` VALUES ('4028888160297cb001602982f6780003', '地主封顶', null, 'limit', null, 'radio', null, 'beimi', null, '2017-12-07 09:48:50', '2017-12-07 09:48:50', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '2', 'four');
INSERT INTO `bm_game_group` VALUES ('4028888160297cb00160298458bb0007', '局数', null, 'games', null, 'radio', null, 'beimi', null, '2017-12-06 23:04:00', '2017-12-06 23:04:00', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '2', 'three');
INSERT INTO `bm_game_group` VALUES ('40288881602a0f1701602a100cba0000', '玩法', null, 'game', null, 'radio', null, 'beimi', null, '2017-12-06 23:03:12', '2017-12-06 23:03:12', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '0', 'four');
INSERT INTO `bm_game_group` VALUES ('402888816045059b016045b092610000', '模式', null, 'model', null, 'checkbox', null, 'beimi', null, '2017-12-11 21:08:08', '2017-12-11 21:08:08', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '0', 'four');
INSERT INTO `bm_game_group` VALUES ('402888816045059b016045b2d3de0006', '加倍', null, 'rota', null, 'radio', null, 'beimi', null, '2017-12-11 21:10:36', '2017-12-11 21:10:36', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '0', 'four');
INSERT INTO `bm_game_group` VALUES ('402888816045059b016045b5691c000a', '局数', null, 'games', null, 'radio', null, 'beimi', null, '2017-12-11 21:14:28', '2017-12-11 21:14:28', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '0', 'three');

-- ----------------------------
-- Table structure for `bm_game_groupitem`
-- ----------------------------
DROP TABLE IF EXISTS `bm_game_groupitem`;
CREATE TABLE `bm_game_groupitem` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `TITLE` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(32) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `STATUS` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `CREATER` varchar(255) DEFAULT NULL,
  `PLAYWAYID` varchar(32) DEFAULT NULL,
  `GAME` varchar(32) DEFAULT NULL,
  `DEFAULTVALUE` tinyint(4) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT '0',
  UNIQUE KEY `SQL121227155530400` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_game_groupitem
-- ----------------------------
INSERT INTO `bm_game_groupitem` VALUES ('40288881602977a40160297a6bb20001', '无赖子', null, 'wu', '40288881602977a40160297a42aa0000', null, null, 'beimi', null, '2017-12-06 09:39:37', '2017-12-06 09:39:37', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '0', 'wu', '0');
INSERT INTO `bm_game_groupitem` VALUES ('4028888160297cb00160298249670001', '抢地主', null, 'qiang', '4028888160297cb001602980d6180000', null, null, 'beimi', null, '2017-12-06 09:48:13', '2017-12-06 09:48:13', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '1', 'qiang', '0');
INSERT INTO `bm_game_groupitem` VALUES ('4028888160297cb001602982931e0002', '叫分', null, 'score', '4028888160297cb001602980d6180000', null, null, 'beimi', null, '2017-12-06 09:48:31', '2017-12-06 09:48:31', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '0', 'score', '0');
INSERT INTO `bm_game_groupitem` VALUES ('4028888160297cb0016029834ece0004', '64分', null, 'limit1', '4028888160297cb001602982f6780003', null, null, 'beimi', null, '2017-12-06 09:49:19', '2017-12-06 09:49:19', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '0', '64', '0');
INSERT INTO `bm_game_groupitem` VALUES ('4028888160297cb00160298381550005', '128分', null, 'limit2', '4028888160297cb001602982f6780003', null, null, 'beimi', null, '2017-12-06 09:49:32', '2017-12-06 09:49:32', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '0', '128', '0');
INSERT INTO `bm_game_groupitem` VALUES ('4028888160297cb001602983b76b0006', '256分', null, 'limit3', '4028888160297cb001602982f6780003', null, null, 'beimi', null, '2017-12-06 09:49:46', '2017-12-06 09:49:46', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '1', '256', '0');
INSERT INTO `bm_game_groupitem` VALUES ('4028888160297cb001602984c30b0008', '8局（房卡x1）', null, 'games1', '4028888160297cb00160298458bb0007', null, null, 'beimi', null, '2017-12-06 09:50:55', '2017-12-06 09:50:55', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '0', '8', '0');
INSERT INTO `bm_game_groupitem` VALUES ('4028888160297cb0016029852b590009', '16局（房卡x2）', null, 'games2', '4028888160297cb00160298458bb0007', null, null, 'beimi', null, '2017-12-06 09:51:21', '2017-12-06 09:51:21', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '0', '16', '0');
INSERT INTO `bm_game_groupitem` VALUES ('4028888160297cb001602985aebf000a', '25局（房卡x3）', null, 'games3', '4028888160297cb00160298458bb0007', null, null, 'beimi', null, '2017-12-06 09:51:55', '2017-12-06 09:51:55', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '1', '25', '0');
INSERT INTO `bm_game_groupitem` VALUES ('402888816029f141016029f359510002', '红中癞子', null, 'hong', '40288881602977a40160297a42aa0000', null, null, 'beimi', null, '2017-12-06 11:51:42', '2017-12-06 11:51:42', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '1', 'hong', '0');
INSERT INTO `bm_game_groupitem` VALUES ('40288881602a0f1701602a106a530001', '经典玩法', null, 'dizhu', '40288881602a0f1701602a100cba0000', null, null, 'beimi', null, '2017-12-06 12:23:27', '2017-12-06 12:23:27', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '1', 'dizhu', '0');
INSERT INTO `bm_game_groupitem` VALUES ('40288881602a0f1701602a109f490002', '癞子玩法', null, 'laizi', '40288881602a0f1701602a100cba0000', null, null, 'beimi', null, '2017-12-06 12:23:41', '2017-12-06 12:23:41', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba5fe7d0002', '402888815fe3f44a015feba097ce0000', '0', 'laizi', '1');
INSERT INTO `bm_game_groupitem` VALUES ('402888816045059b016045b0fc420001', '无字牌', null, 'wind', '402888816045059b016045b092610000', null, null, 'beimi', null, '2017-12-11 21:08:35', '2017-12-11 21:08:35', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '0', 'true', '0');
INSERT INTO `bm_game_groupitem` VALUES ('402888816045059b016045b17f5c0002', '可以吃牌', null, 'chi', '402888816045059b016045b092610000', null, null, 'beimi', null, '2017-12-11 21:09:09', '2017-12-11 21:09:09', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '0', 'chi', '0');
INSERT INTO `bm_game_groupitem` VALUES ('402888816045059b016045b1c77a0003', '可抢杠胡', null, 'ganghu', '402888816045059b016045b092610000', null, null, 'beimi', null, '2017-12-11 21:09:27', '2017-12-11 21:09:27', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '1', 'ganghu', '0');
INSERT INTO `bm_game_groupitem` VALUES ('402888816045059b016045b1fa4f0004', '可点炮胡', null, 'dian', '402888816045059b016045b092610000', null, null, 'beimi', null, '2017-12-11 21:09:40', '2017-12-11 21:09:40', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '0', 'dian', '0');
INSERT INTO `bm_game_groupitem` VALUES ('402888816045059b016045b258ac0005', '可胡七对', null, 'qi', '402888816045059b016045b092610000', null, null, 'beimi', null, '2017-12-11 21:10:04', '2017-12-11 21:10:04', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '1', 'qi', '0');
INSERT INTO `bm_game_groupitem` VALUES ('402888816045059b016045b30e230007', '跟庄', null, 'zhuang', '402888816045059b016045b2d3de0006', null, null, 'beimi', null, '2017-12-11 21:10:51', '2017-12-11 21:10:51', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '1', 'zhuang', '0');
INSERT INTO `bm_game_groupitem` VALUES ('402888816045059b016045b356500008', '杠开x2', null, 'gang', '402888816045059b016045b2d3de0006', null, null, 'beimi', null, '2017-12-11 21:11:09', '2017-12-11 21:11:09', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '0', 'gang', '0');
INSERT INTO `bm_game_groupitem` VALUES ('402888816045059b016045b4c6ce0009', '抢杠胡', null, 'qiang', '402888816045059b016045b2d3de0006', null, null, 'beimi', null, '2017-12-11 21:12:43', '2017-12-11 21:12:43', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '0', 'qiang', '0');
INSERT INTO `bm_game_groupitem` VALUES ('402888816045059b016045b5fb3c000b', '8局（房卡x1）', null, 'games1', '402888816045059b016045b5691c000a', null, null, 'beimi', null, '2017-12-11 21:14:02', '2017-12-11 21:14:02', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '0', '8', '0');
INSERT INTO `bm_game_groupitem` VALUES ('402888816045059b016045b622a1000c', '16局（房卡x2）', null, 'games2', '402888816045059b016045b5691c000a', null, null, 'beimi', null, '2017-12-11 23:38:34', '2017-12-11 23:38:40', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '1', '16', '0');
INSERT INTO `bm_game_groupitem` VALUES ('402888816045059b016045b647e6000d', '25局（房卡x3）', null, 'games3', '402888816045059b016045b5691c000a', null, null, 'beimi', null, '2017-12-11 21:14:22', '2017-12-11 21:14:22', '297e8c7b455798280145579c73e501c1', '402888815fe3f44a015feba86a330003', '402888815fe3f44a015feba097ce0000', '0', '25', '0');

-- ----------------------------
-- Table structure for `bm_game_mjcardstype`
-- ----------------------------
DROP TABLE IF EXISTS `bm_game_mjcardstype`;
CREATE TABLE `bm_game_mjcardstype` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(50) DEFAULT NULL COMMENT '代码',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '用户名',
  `PARENTID` varchar(32) DEFAULT NULL COMMENT '上级分类ID',
  `GAME` varchar(32) DEFAULT NULL,
  `GANG` varchar(32) DEFAULT NULL,
  `PENG` varchar(32) DEFAULT NULL,
  `CHI` varchar(32) DEFAULT NULL,
  `PAIR` varchar(32) DEFAULT NULL,
  `SELF` tinyint(32) DEFAULT NULL,
  `LAST` tinyint(4) DEFAULT NULL,
  `LASTGANG` tinyint(4) DEFAULT NULL,
  `RATE` int(11) DEFAULT NULL,
  `CARDSTYPE` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_game_mjcardstype
-- ----------------------------

-- ----------------------------
-- Table structure for `bm_game_playway`
-- ----------------------------
DROP TABLE IF EXISTS `bm_game_playway`;
CREATE TABLE `bm_game_playway` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(50) DEFAULT NULL COMMENT '代码',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '用户名',
  `area` text,
  `parentid` varchar(32) DEFAULT '0',
  `typeid` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `game` varchar(32) DEFAULT NULL,
  `players` int(11) DEFAULT NULL,
  `cards` int(11) DEFAULT NULL,
  `roomtype` varchar(32) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `mincoins` int(11) DEFAULT NULL,
  `maxcoins` int(11) DEFAULT NULL,
  `changecard` tinyint(4) DEFAULT NULL,
  `shuffle` tinyint(4) DEFAULT NULL,
  `numofgames` int(11) DEFAULT NULL,
  `cardsnum` int(11) DEFAULT NULL,
  `typelevel` varchar(20) DEFAULT NULL,
  `typecolor` varchar(20) DEFAULT NULL,
  `sortindex` int(11) DEFAULT NULL,
  `powerful` varchar(32) DEFAULT NULL,
  `shuffletimes` int(11) DEFAULT NULL,
  `wind` tinyint(4) DEFAULT NULL,
  `memo` varchar(200) DEFAULT NULL,
  `free` tinyint(4) DEFAULT '0',
  `roomtitle` varchar(50) DEFAULT NULL,
  `extpro` tinyint(4) DEFAULT '0',
  `cardsrules` text,
  `mjwinrules` text,
  `wintype` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `bm_game_room`
-- ----------------------------
DROP TABLE IF EXISTS `bm_game_room`;
CREATE TABLE `bm_game_room` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(50) DEFAULT NULL COMMENT '代码',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '用户名',
  `area` text,
  `parentid` varchar(32) DEFAULT '0',
  `typeid` varchar(32) DEFAULT NULL,
  `roomid` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `game` varchar(32) DEFAULT NULL,
  `players` int(11) DEFAULT NULL,
  `master` varchar(32) DEFAULT NULL,
  `roomtype` varchar(32) DEFAULT NULL,
  `playway` varchar(32) DEFAULT NULL,
  `numofgames` int(11) DEFAULT NULL,
  `currentnum` int(11) DEFAULT NULL,
  `curpalyers` int(11) DEFAULT NULL,
  `cardroom` tinyint(4) DEFAULT NULL,
  `cardsnum` int(11) DEFAULT NULL,
  `matchmodel` tinyint(4) DEFAULT NULL,
  `matchid` varchar(32) DEFAULT NULL,
  `matchscreen` int(11) DEFAULT NULL,
  `matchtype` varchar(32) DEFAULT NULL,
  `lastwinner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `bm_generation`
-- ----------------------------
DROP TABLE IF EXISTS `bm_generation`;
CREATE TABLE `bm_generation` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `model` varchar(32) DEFAULT NULL COMMENT '所属组件',
  `startinx` int(11) DEFAULT NULL COMMENT '开始位置',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `bm_organ`
-- ----------------------------
DROP TABLE IF EXISTS `bm_organ`;
CREATE TABLE `bm_organ` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(50) DEFAULT NULL COMMENT '代码',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '用户名',
  `PARENT` varchar(32) DEFAULT NULL COMMENT '父级ID',
  `SKILL` tinyint(4) DEFAULT '0' COMMENT '启用技能组',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `bm_organrole`
-- ----------------------------
DROP TABLE IF EXISTS `bm_organrole`;
CREATE TABLE `bm_organrole` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `organ_id` varchar(32) DEFAULT NULL COMMENT '机构ID',
  `role_id` varchar(32) DEFAULT NULL COMMENT '角色ID',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_organrole
-- ----------------------------

-- ----------------------------
-- Table structure for `bm_playuser`
-- ----------------------------
DROP TABLE IF EXISTS `bm_playuser`;
CREATE TABLE `bm_playuser` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `LANGUAGE` varchar(255) DEFAULT NULL COMMENT '语言',
  `USERNAME` varchar(255) DEFAULT NULL COMMENT '用户名',
  `PASSWORD` varchar(255) DEFAULT NULL COMMENT '密码',
  `SECURECONF` varchar(255) DEFAULT NULL COMMENT '安全级别',
  `EMAIL` varchar(255) DEFAULT NULL COMMENT '邮件',
  `FIRSTNAME` varchar(255) DEFAULT NULL COMMENT '姓',
  `MIDNAME` varchar(255) DEFAULT NULL COMMENT '名',
  `LASTNAME` varchar(255) DEFAULT NULL COMMENT '名',
  `JOBTITLE` varchar(255) DEFAULT NULL COMMENT '职位',
  `DEPARTMENT` varchar(255) DEFAULT NULL COMMENT '部门',
  `GENDER` varchar(255) DEFAULT NULL COMMENT '性别',
  `BIRTHDAY` varchar(255) DEFAULT NULL COMMENT '生日',
  `NICKNAME` varchar(255) DEFAULT NULL COMMENT '昵称',
  `USERTYPE` varchar(255) DEFAULT NULL COMMENT '用户类型',
  `RULENAME` varchar(255) DEFAULT NULL COMMENT '角色',
  `SEARCHPROJECTID` varchar(255) DEFAULT NULL COMMENT '备用',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `MEMO` varchar(255) DEFAULT NULL COMMENT '备注',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `MOBILE` varchar(32) DEFAULT NULL COMMENT '手机号',
  `passupdatetime` datetime DEFAULT NULL COMMENT '最后 一次密码修改时间',
  `sign` varchar(100) DEFAULT NULL,
  `del` tinyint(4) DEFAULT '0',
  `login` tinyint(4) DEFAULT '0',
  `online` tinyint(4) DEFAULT '0',
  `headimg` tinyint(4) DEFAULT '0' COMMENT '是否设置头像',
  `secquestion` tinyint(4) DEFAULT '0' COMMENT '是否设置密保问题',
  `playerlevel` varchar(32) DEFAULT NULL COMMENT '玩家等级',
  `experience` int(11) DEFAULT '0' COMMENT '玩家积分',
  `uname` varchar(100) DEFAULT NULL,
  `musteditpassword` tinyint(4) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL COMMENT '省份',
  `city` varchar(50) DEFAULT NULL COMMENT '城市',
  `fans` int(11) DEFAULT '0' COMMENT '关注人数',
  `follows` int(11) DEFAULT '0' COMMENT '被关注次数',
  `integral` int(11) DEFAULT '0',
  `lastlogintime` datetime DEFAULT NULL COMMENT '最后登录时间',
  `status` varchar(10) DEFAULT NULL COMMENT '状态',
  `deactivetime` datetime DEFAULT NULL COMMENT '离线时间',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `DATASTATUS` tinyint(4) DEFAULT '0' COMMENT '数据状态',
  `token` varchar(50) DEFAULT NULL,
  `cards` int(11) DEFAULT NULL,
  `goldcoins` int(11) DEFAULT NULL,
  `diamonds` int(11) DEFAULT NULL,
  `openid` varchar(100) DEFAULT NULL,
  `qqid` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `region` varchar(200) DEFAULT NULL,
  `isp` varchar(100) DEFAULT NULL,
  `ostype` varchar(32) DEFAULT NULL,
  `disabled` tinyint(4) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  `browser` varchar(100) DEFAULT NULL,
  `playertype` varchar(50) DEFAULT NULL,
  `gamestatus` varchar(50) DEFAULT NULL,
  `roomid` varchar(32) DEFAULT NULL,
  `roomready` tinyint(4) DEFAULT '0',
  `opendeal` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `bm_project`
-- ----------------------------
DROP TABLE IF EXISTS `bm_project`;
CREATE TABLE `bm_project` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `contexturl` longtext,
  `multilang` smallint(6) NOT NULL,
  `langtype` varchar(255) DEFAULT NULL,
  `packagename` varchar(255) DEFAULT NULL,
  `dbid` varchar(255) DEFAULT NULL,
  `creater` varchar(32) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `custom` tinyint(4) DEFAULT NULL,
  `accessafterlogin` tinyint(4) DEFAULT NULL,
  `params` varchar(255) DEFAULT NULL,
  `toptemplet` varchar(255) DEFAULT NULL,
  `lefttemplet` varchar(255) DEFAULT NULL,
  `description` text,
  `teamid` varchar(32) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_project
-- ----------------------------
INSERT INTO `bm_project` VALUES ('40288881601f44eb01601fc9361b0002', '网贷催收系统', 'beimi', 'cuishou', null, null, '0', null, null, null, '297e8c7b455798280145579c73e501c1', '2017-12-04 12:29:28', '2017-12-04 12:29:28', '0', '0', null, null, null, '电话催收系统，名单清洗、分配，M1~12催收，自动名单分配电话催收系统，名单清洗、分配，M1~12催收，自动名单分配电话催收系统，名单清洗、分配，M1~12催收，自动名单分配', null, null);
INSERT INTO `bm_project` VALUES ('40288881601f44eb01601fd0c8be0003', '号码盾牌系统', 'beimi', 'dun', null, null, '0', null, null, null, '297e8c7b455798280145579c73e501c1', '2017-12-04 12:37:45', '2017-12-04 12:37:45', '0', '0', null, null, null, '网约车号码盾牌，对接第三方系统，包含对接接口和运营监控系统，网约车号码盾牌，对接第三方系统，包含对接接口和运营监控系统', null, null);
INSERT INTO `bm_project` VALUES ('40288881601f44eb01601fd19f730004', '客户资料管理系统', 'beimi', 'customer', null, null, '0', null, null, null, '297e8c7b455798280145579c73e501c1', '2017-12-04 12:38:40', '2017-12-04 12:38:40', '0', '0', null, null, null, '联系人信息统一管理，CRM系统基础功能，为各个系统提供客户信息视图，联系人信息统一管理，CRM系统基础功能，为各个系统提供客户信息视图', null, null);
INSERT INTO `bm_project` VALUES ('40288881601f44eb01601fd374770005', 'asdfa', 'beimi', 'sdfasdfa', null, null, '0', null, null, null, '297e8c7b455798280145579c73e501c1', '2017-12-04 12:40:40', '2017-12-04 12:40:40', '0', '0', null, null, null, 'sdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf', null, null);

-- ----------------------------
-- Table structure for `bm_role`
-- ----------------------------
DROP TABLE IF EXISTS `bm_role`;
CREATE TABLE `bm_role` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(50) DEFAULT NULL COMMENT '代码',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '用户名',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_role
-- ----------------------------
INSERT INTO `bm_role` VALUES ('402888815e026a43015e02aba80f0000', 'ces', null, '2017-08-21 10:42:38', '297e8c7b455798280145579c73e501c1', '2017-08-21 10:42:42', 'ukewo', null);

-- ----------------------------
-- Table structure for `bm_role_auth`
-- ----------------------------
DROP TABLE IF EXISTS `bm_role_auth`;
CREATE TABLE `bm_role_auth` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(50) DEFAULT NULL COMMENT '代码',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '用户名',
  `ROLEID` varchar(32) DEFAULT NULL COMMENT '角色ID',
  `DICID` varchar(32) DEFAULT NULL COMMENT '权限ID',
  `DICVALUE` varchar(30) DEFAULT NULL COMMENT '权限代码',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_role_auth
-- ----------------------------

-- ----------------------------
-- Table structure for `bm_secret`
-- ----------------------------
DROP TABLE IF EXISTS `bm_secret`;
CREATE TABLE `bm_secret` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `createtime` varchar(32) DEFAULT NULL COMMENT '创建时间',
  `password` varchar(100) DEFAULT NULL COMMENT '二次密码',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `model` varchar(32) DEFAULT NULL COMMENT '所属组件',
  `enable` tinyint(4) DEFAULT NULL COMMENT '是否启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `bm_sysdic`
-- ----------------------------
DROP TABLE IF EXISTS `bm_sysdic`;
CREATE TABLE `bm_sysdic` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `NAME` varchar(100) DEFAULT NULL COMMENT '字典名称',
  `TITLE` varchar(100) DEFAULT NULL COMMENT '标题',
  `CODE` varchar(100) DEFAULT NULL COMMENT '代码',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `CTYPE` varchar(32) DEFAULT NULL COMMENT '类型',
  `PARENTID` varchar(32) DEFAULT NULL COMMENT '父级ID',
  `DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '描述',
  `MEMO` varchar(32) DEFAULT NULL COMMENT '备注',
  `ICONSTR` varchar(255) DEFAULT NULL COMMENT '图标',
  `ICONSKIN` varchar(255) DEFAULT NULL COMMENT '自定义样式',
  `CATETYPE` varchar(32) DEFAULT NULL COMMENT '类型',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `HASCHILD` tinyint(4) DEFAULT NULL COMMENT '是否有下级',
  `SORTINDEX` int(11) DEFAULT NULL COMMENT '排序',
  `DICID` varchar(32) DEFAULT NULL COMMENT '目录ID',
  `DEFAULTVALUE` tinyint(4) DEFAULT NULL COMMENT '默认值',
  `DISCODE` tinyint(4) DEFAULT NULL COMMENT '编码',
  `URL` varchar(255) DEFAULT NULL COMMENT '系统权限资源的URL',
  `MODULE` varchar(32) DEFAULT NULL COMMENT '权限资源所属模块',
  `MLEVEL` varchar(32) DEFAULT NULL COMMENT '菜单级别（一级/二级）',
  `RULES` varchar(100) DEFAULT NULL,
  `MENUTYPE` varchar(32) DEFAULT NULL COMMENT '菜单类型（顶部菜单/左侧菜单）',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532210` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
- ----------------------------
-- Table structure for `bm_systemconfig`
-- ----------------------------
DROP TABLE IF EXISTS `bm_systemconfig`;
CREATE TABLE `bm_systemconfig` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `NAME` varchar(100) DEFAULT NULL COMMENT '名称',
  `TITLE` varchar(100) DEFAULT NULL COMMENT '标题',
  `CODE` varchar(100) DEFAULT NULL COMMENT '编码',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `CTYPE` varchar(32) DEFAULT NULL COMMENT '类型',
  `PARENTID` varchar(32) DEFAULT NULL COMMENT '父级ID',
  `DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '描述',
  `MEMO` varchar(32) DEFAULT NULL,
  `ICONSTR` varchar(255) DEFAULT NULL COMMENT '自定义样式',
  `ICONSKIN` varchar(255) DEFAULT NULL COMMENT '自定义样式',
  `CATETYPE` varchar(32) DEFAULT NULL COMMENT '分类',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `HASCHILD` tinyint(4) DEFAULT NULL COMMENT '是否有下级',
  `SORTINDEX` int(11) DEFAULT NULL COMMENT '排序',
  `DICID` varchar(32) DEFAULT NULL COMMENT '目录ID',
  `DEFAULTVALUE` tinyint(4) DEFAULT NULL COMMENT '默认值',
  `THEME` varchar(50) DEFAULT NULL COMMENT '皮肤',
  `LOGLEVEL` varchar(32) DEFAULT NULL COMMENT '日志级别',
  `ENABLESSL` tinyint(4) DEFAULT NULL COMMENT '启用SSL',
  `JKSFILE` varchar(255) DEFAULT NULL COMMENT 'JKS文件路径',
  `JKSPASSWORD` varchar(255) DEFAULT NULL COMMENT 'JKS密码',
  `MAPKEY` varchar(255) DEFAULT NULL COMMENT '百度地图授权编码',
  `workorders` tinyint(4) DEFAULT NULL COMMENT '启用工单三栏布局',
  `callcenter` tinyint(4) DEFAULT NULL COMMENT '启用呼叫中心',
  `cc_extention` varchar(32) DEFAULT NULL COMMENT '分机',
  `cc_quene` varchar(32) DEFAULT NULL COMMENT '技能组队列',
  `cc_router` varchar(32) DEFAULT NULL COMMENT '路由策略',
  `cc_ivr` varchar(32) DEFAULT NULL COMMENT 'IVR模板',
  `cc_acl` varchar(32) DEFAULT NULL COMMENT '访问列表模板',
  `cc_siptrunk` varchar(32) DEFAULT NULL COMMENT 'SIP配置模板',
  `cc_callcenter` varchar(32) DEFAULT NULL COMMENT '呼叫中心配置',
  `CALLOUT` tinyint(4) DEFAULT NULL COMMENT '是否允许点击号码外呼',
  `AUTH` tinyint(4) DEFAULT NULL COMMENT '启用权限控制',
  UNIQUE KEY `SQL121227155532210` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `bm_tableproperties`
-- ----------------------------
DROP TABLE IF EXISTS `bm_tableproperties`;
CREATE TABLE `bm_tableproperties` (
  `ID` varchar(32) NOT NULL DEFAULT '' COMMENT '主键ID',
  `NAME` varchar(255) DEFAULT NULL COMMENT '字段名称',
  `CODE` varchar(255) DEFAULT NULL COMMENT '代码',
  `GROUPID` varchar(255) DEFAULT NULL COMMENT '组ID',
  `USERID` varchar(255) DEFAULT NULL COMMENT '创建人ID',
  `FIELDNAME` varchar(255) DEFAULT NULL COMMENT '字段名称',
  `DATATYPECODE` int(11) NOT NULL COMMENT '数据类型代码',
  `DATATYPENAME` varchar(255) DEFAULT NULL COMMENT '字段类型名称',
  `DBTABLEID` varchar(255) DEFAULT NULL COMMENT '数据表ID',
  `INDEXDATATYPE` varchar(255) DEFAULT NULL COMMENT '字段类型',
  `PK` smallint(6) DEFAULT NULL COMMENT '是否外键',
  `MODITS` smallint(6) DEFAULT NULL,
  `INDEXFIELD` varchar(32) DEFAULT NULL COMMENT '是否索引',
  `PLUGIN` varchar(32) DEFAULT NULL COMMENT '处理插件',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `FKTABLE` varchar(32) DEFAULT NULL COMMENT '外键表',
  `FKPROPERTY` varchar(32) DEFAULT NULL COMMENT '外键字段',
  `TABLENAME` varchar(255) DEFAULT NULL COMMENT '数据表名称',
  `viewtype` varchar(255) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT NULL COMMENT '排序位置',
  `SYSTEMFIELD` tinyint(4) DEFAULT NULL COMMENT '系统字段',
  `INX` tinyint(4) DEFAULT NULL COMMENT '索引',
  `TOKEN` tinyint(4) DEFAULT NULL COMMENT '分词',
  `LENGTH` int(11) DEFAULT NULL COMMENT '长度',
  `FIELDSTATUS` tinyint(4) DEFAULT NULL COMMENT '字段状态',
  `SELDATA` tinyint(4) DEFAULT NULL COMMENT '关联字段数据',
  `SELDATACODE` varchar(32) DEFAULT NULL COMMENT '关联字段代码',
  `SELDATAKEY` varchar(32) DEFAULT NULL COMMENT '关联key',
  `SELDATAVALUE` varchar(32) DEFAULT NULL COMMENT '关联字段值',
  `SELDATATYPE` varchar(32) DEFAULT NULL COMMENT '关联类型',
  `REFTBID` varchar(32) DEFAULT NULL COMMENT '引用表ID',
  `REFTPID` varchar(32) DEFAULT NULL COMMENT '引用字段ID',
  `REFTYPE` varchar(32) DEFAULT NULL COMMENT '引用类型',
  `REFTBNAME` varchar(60) DEFAULT NULL COMMENT '引用表名称',
  `REFTPNAME` varchar(60) DEFAULT NULL,
  `REFTPTITLEFIELD` varchar(60) DEFAULT NULL,
  `REFFK` tinyint(4) DEFAULT NULL,
  `DEFAULTSORT` tinyint(4) DEFAULT NULL,
  `DEFAULTVALUE` varchar(255) DEFAULT NULL,
  `DEFAULTVALUETITLE` varchar(255) DEFAULT NULL,
  `DEFAULTFIELDVALUE` varchar(255) DEFAULT NULL,
  `MULTPARTFILE` tinyint(4) DEFAULT NULL,
  `UPLOADTYPE` varchar(255) DEFAULT NULL,
  `cascadetype` varchar(255) DEFAULT NULL,
  `title` tinyint(4) DEFAULT NULL,
  `DESCORDER` tinyint(4) DEFAULT NULL,
  `impfield` tinyint(4) DEFAULT '0',
  `tokentype` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL130112140848940` (`ID`) USING BTREE,
  KEY `FKF8D74787854BC62` (`DBTABLEID`) USING BTREE,
  KEY `FKF8D747811BE44FF` (`FKPROPERTY`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `bm_tabletask`
-- ----------------------------
DROP TABLE IF EXISTS `bm_tabletask`;
CREATE TABLE `bm_tabletask` (
  `ID` varchar(32) NOT NULL DEFAULT '',
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `TABLEDIRID` varchar(255) DEFAULT NULL,
  `DBID` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `CREATERNAME` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` text,
  `LISTBLOCKTEMPLET` text,
  `TABLENAME` varchar(255) DEFAULT NULL,
  `TABLETYPE` varchar(255) DEFAULT NULL,
  `STARTINDEX` int(11) NOT NULL,
  `UPDATETIME` datetime DEFAULT NULL,
  `UPDATETIMENUMBER` int(11) NOT NULL,
  `DATASQL` longtext,
  `DATABASETASK` varchar(32) DEFAULT NULL,
  `DRIVERPLUGIN` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `WORKFLOW` tinyint(10) DEFAULT NULL,
  `FROMDB` tinyint(4) DEFAULT NULL,
  `tabtype` varchar(32) DEFAULT NULL,
  `pid` varchar(32) DEFAULT NULL,
  `secmenuid` varchar(32) DEFAULT NULL,
  `reportid` varchar(32) DEFAULT NULL,
  `eventname` varchar(32) DEFAULT NULL,
  `tltemplet` varchar(32) DEFAULT NULL,
  `timeline` tinyint(4) DEFAULT NULL,
  `tbversion` int(11) DEFAULT NULL,
  `LASTUPDATE` datetime DEFAULT NULL,
  `CREATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `bm_team`
-- ----------------------------
DROP TABLE IF EXISTS `bm_team`;
CREATE TABLE `bm_team` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(50) DEFAULT NULL COMMENT '代码',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '用户名',
  `PARENT` varchar(32) DEFAULT NULL COMMENT '父级ID',
  `SKILL` tinyint(4) DEFAULT '0' COMMENT '启用技能组',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `bm_templet`
-- ----------------------------
DROP TABLE IF EXISTS `bm_templet`;
CREATE TABLE `bm_templet` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `NAME` varchar(255) DEFAULT NULL COMMENT '模板名称',
  `DESCRIPTION` longtext COMMENT '描述',
  `CODE` varchar(255) DEFAULT NULL COMMENT '代码',
  `GROUPID` varchar(255) DEFAULT NULL COMMENT '组ID',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `USERID` varchar(255) DEFAULT NULL COMMENT '创建人ID',
  `TEMPLETTEXT` longtext COMMENT '模板内容',
  `TEMPLETTYPE` varchar(255) DEFAULT NULL COMMENT '模板类型',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `ICONSTR` varchar(255) DEFAULT NULL COMMENT '自定义样式',
  `MEMO` varchar(255) DEFAULT NULL COMMENT '备注',
  `ORDERINDEX` int(11) DEFAULT NULL COMMENT '排序位置',
  `TYPEID` varchar(32) DEFAULT NULL COMMENT '分类ID',
  `SELDATA` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bm_templet
-- ----------------------------

-- ----------------------------
-- Table structure for `bm_user`
-- ----------------------------
DROP TABLE IF EXISTS `bm_user`;
CREATE TABLE `bm_user` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `LANGUAGE` varchar(255) DEFAULT NULL COMMENT '语言',
  `USERNAME` varchar(255) DEFAULT NULL COMMENT '用户名',
  `PASSWORD` varchar(255) DEFAULT NULL COMMENT '密码',
  `SECURECONF` varchar(255) DEFAULT NULL COMMENT '安全级别',
  `EMAIL` varchar(255) DEFAULT NULL COMMENT '邮件',
  `FIRSTNAME` varchar(255) DEFAULT NULL COMMENT '姓',
  `MIDNAME` varchar(255) DEFAULT NULL COMMENT '名',
  `LASTNAME` varchar(255) DEFAULT NULL COMMENT '名',
  `JOBTITLE` varchar(255) DEFAULT NULL COMMENT '职位',
  `DEPARTMENT` varchar(255) DEFAULT NULL COMMENT '部门',
  `GENDER` varchar(255) DEFAULT NULL COMMENT '性别',
  `BIRTHDAY` varchar(255) DEFAULT NULL COMMENT '生日',
  `NICKNAME` varchar(255) DEFAULT NULL COMMENT '昵称',
  `USERTYPE` varchar(255) DEFAULT NULL COMMENT '用户类型',
  `RULENAME` varchar(255) DEFAULT NULL COMMENT '角色',
  `SEARCHPROJECTID` varchar(255) DEFAULT NULL COMMENT '备用',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `MEMO` varchar(255) DEFAULT NULL COMMENT '备注',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGAN` varchar(32) DEFAULT NULL COMMENT '部门',
  `MOBILE` varchar(32) DEFAULT NULL COMMENT '手机号',
  `passupdatetime` datetime DEFAULT NULL COMMENT '最后 一次密码修改时间',
  `sign` text,
  `del` tinyint(4) DEFAULT '0',
  `uname` varchar(100) DEFAULT NULL,
  `musteditpassword` tinyint(4) DEFAULT NULL,
  `AGENT` tinyint(4) DEFAULT NULL COMMENT '工号',
  `SKILL` varchar(32) DEFAULT NULL COMMENT '技能组',
  `province` varchar(50) DEFAULT NULL COMMENT '省份',
  `city` varchar(50) DEFAULT NULL COMMENT '城市',
  `fans` int(11) DEFAULT NULL COMMENT '关注人数',
  `follows` int(11) DEFAULT NULL COMMENT '被关注次数',
  `integral` int(11) DEFAULT NULL,
  `lastlogintime` datetime DEFAULT NULL COMMENT '最后登录时间',
  `status` varchar(10) DEFAULT NULL COMMENT '状态',
  `deactivetime` datetime DEFAULT NULL COMMENT '离线时间',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `DATASTATUS` tinyint(4) DEFAULT NULL COMMENT '数据状态',
  `callcenter` tinyint(4) DEFAULT NULL COMMENT '启用呼叫中心坐席',
  `SUPERUSER` tinyint(4) DEFAULT NULL COMMENT '是否超级管理员',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `bm_userrole`
-- ----------------------------
DROP TABLE IF EXISTS `bm_userrole`;
CREATE TABLE `bm_userrole` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `role_id` varchar(32) DEFAULT NULL COMMENT '角色ID',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `bm_wares`
-- ----------------------------
DROP TABLE IF EXISTS `bm_wares`;
CREATE TABLE `bm_wares` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL,
  `CODE` varchar(50) DEFAULT NULL,
  `CREATETIME` datetime DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `UPDATETIME` datetime DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `USERNAME` varchar(50) DEFAULT NULL,
  `WARESTYPE` varchar(32) DEFAULT NULL COMMENT '知识库分类上级ID',
  `INX` int(11) DEFAULT NULL COMMENT '分类排序序号',
  `STARTDATE` datetime DEFAULT NULL COMMENT '有效期开始时间',
  `DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '分类描述',
  `PRICE` int(11) DEFAULT NULL,
  `STOCK` int(11) DEFAULT NULL,
  `IMAGEURL` varchar(50) DEFAULT NULL,
  `PMETHOD` varchar(32) DEFAULT NULL,
  `SHOPID` varchar(32) DEFAULT NULL,
  `SPU` int(11) DEFAULT NULL,
  `STATUS` varchar(32) DEFAULT NULL,
  `RECOMTYPE` varchar(32) DEFAULT NULL,
  `ENABLESKU` tinyint(4) DEFAULT NULL,
  `PAYMENT` varchar(32) DEFAULT NULL,
  `ENDTIME` datetime DEFAULT NULL,
  `STARTTIME` datetime DEFAULT NULL,
  `QUANTITY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for `bm_wares_sku`
-- ----------------------------
DROP TABLE IF EXISTS `bm_wares_sku`;
CREATE TABLE `bm_wares_sku` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL,
  `CODE` varchar(50) DEFAULT NULL,
  `CREATETIME` datetime DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `UPDATETIME` datetime DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `USERNAME` varchar(50) DEFAULT NULL,
  `WARESID` varchar(32) DEFAULT NULL COMMENT '知识库分类上级ID',
  `INX` int(11) DEFAULT NULL COMMENT '分类排序序号',
  `STARTDATE` datetime DEFAULT NULL COMMENT '有效期开始时间',
  `DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '分类描述',
  `PRICE` int(11) DEFAULT NULL,
  `STOCK` int(11) DEFAULT NULL,
  `IMAGEURL` varchar(50) DEFAULT NULL,
  `PMETHOD` varchar(32) DEFAULT NULL,
  `SHOPID` varchar(32) DEFAULT NULL,
  `SKU` int(11) DEFAULT NULL,
  `STATUS` varchar(32) DEFAULT NULL,
  `DEFAULTSKU` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for `bm_wares_type`
-- ----------------------------
DROP TABLE IF EXISTS `bm_wares_type`;
CREATE TABLE `bm_wares_type` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL,
  `CODE` varchar(50) DEFAULT NULL,
  `CREATETIME` datetime DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `UPDATETIME` datetime DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `USERNAME` varchar(50) DEFAULT NULL,
  `PARENTID` varchar(32) DEFAULT NULL COMMENT '知识库分类上级ID',
  `INX` int(11) DEFAULT NULL COMMENT '分类排序序号',
  `STARTDATE` datetime DEFAULT NULL COMMENT '有效期开始时间',
  `DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '分类描述',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;


