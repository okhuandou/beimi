cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        bgVolume: 1.0,           // 背景音量

        deskVolume: 1.0,         //   房间 房间音量
        // sfxVolume: 1.0,
        bgAudioID: -1            //   背景 音乐  id
    },

    // use this for initialization
    init: function () {
        var t = cc.sys.localStorage.getItem("bgVolume");
        if (t != null) {
            this.bgVolume = parseFloat(t);
        }

        var t = cc.sys.localStorage.getItem("deskVolume");
        if (t != null) {
            this.deskVolume = parseFloat(t);
        }

        // var t = cc.sys.localStorage.getItem("sfxVolume");
        // if (t != null) {
        //     this.sfxVolume = parseFloat(t);
        // }


        cc.game.on(cc.game.EVENT_HIDE, function () {
            cc.audioEngine.pauseAll();
        });
        cc.game.on(cc.game.EVENT_SHOW, function () {
            cc.audioEngine.resumeAll();
        });
    },

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },

    getUrl: function (url) {
        // return cc.url.raw("resources/sounds/" + url);
        return "sounds/" + url;
    },

    playBGM: function (url) {
        let _self = this;
        var audioUrl = this.getUrl(url);
        console.log(audioUrl);
        if (this.bgAudioID >= 0) {
            cc.audioEngine.stop(this.bgAudioID);
        }
        cc.loader.loadRes(audioUrl, cc.AudioClip, function (err, clip) {
            _self.bgAudioID = cc.audioEngine.play(clip);
            cc.audioEngine.setLoop(_self.bgAudioID, true);
            console.log(_self.bgAudioID);
        });
        // this.bgAudioID = cc.audioEngine.play(audioUrl,true,this.bgVolume);
    },

    playSFX: function (url) {
        var audioUrl = this.getUrl(url);
        var volume = this.deskVolume
        if (this.deskVolume > 0) {
            cc.loader.loadRes(audioUrl, cc.AudioClip, function (err, clip) {
                var id = cc.audioEngine.play(clip);
                cc.audioEngine.setVolume(id, volume);
            });
        }
    },

    setSFXVolume: function (v) {
        if (this.sfxVolume != v) {
            cc.sys.localStorage.setItem("deskVolume", v);
            this.deskVolume = v;
        }
    },
    getState: function () {
        return cc.audioEngine.getState(this.bgAudioID);
    },
    setBGMVolume: function (v, force) {
        if (this.bgAudioID >= 0) {
            if (v > 0 && cc.audioEngine.getState(this.bgAudioID) === cc.audioEngine.AudioState.PAUSED) {
                cc.audioEngine.resume(this.bgAudioID);
            } else if (v == 0) {
                cc.audioEngine.pause(this.bgAudioID);
            }
        }
        if (this.bgVolume != v || force) {
            cc.sys.localStorage.setItem("bgVolume", v);
            this.bgmVolume = v;
            cc.audioEngine.setVolume(this.bgAudioID, v);
        }
    },

    pauseAll: function () {
        cc.audioEngine.pauseAll();
    },

    resumeAll: function () {
        cc.audioEngine.resumeAll();
    }
});
